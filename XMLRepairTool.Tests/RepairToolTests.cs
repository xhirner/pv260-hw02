using System;
using System.IO;
using System.Xml;
using NUnit.Framework;
using XMLRepairTool;

namespace Tests
{
    public class Tests
    {
        public RepairTool _rt;
        public string RawXml;

        [SetUp]
        public void Setup()
        {
            _rt = new RepairTool();
            RawXml = File.ReadAllText(@"C:\Users\Erik\source\repos\XMLRepairTool\XMLRepairTool\test.xml");
        }

        [Test]
        public void RepairRegex_NoExceptionThrown()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(_rt.RemoveInvalidXmlCharactersRegex(RawXml));
            }
            catch (Exception e)
            {
                Assert.Fail("No exception was expected but " + e.Message + "was thrown");
            }
        }

        [Test]
        public void RepairSB_NoExceptionThrown()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(_rt.RemoveInvalidXmlCharactersSB(RawXml));
            }
            catch (Exception e)
            {
                Assert.Fail("No exception was expected but " + e.Message + "was thrown");
            }
        }

        [Test]
        public void RepairArray_NoExceptionThrown()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(_rt.RemoveInvalidXmlCharactersArray(RawXml));
            }
            catch (Exception e)
            {
                Assert.Fail("No exception was expected but " + e.Message + "was thrown");
            }
        }
    }
}