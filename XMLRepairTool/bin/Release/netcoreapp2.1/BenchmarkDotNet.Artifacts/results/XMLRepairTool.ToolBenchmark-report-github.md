``` ini

BenchmarkDotNet=v0.11.5, OS=Windows 10.0.17134.706 (1803/April2018Update/Redstone4)
Intel Core i5-8250U CPU 1.60GHz (Kaby Lake R), 1 CPU, 8 logical and 4 physical cores
Frequency=1757815 Hz, Resolution=568.8881 ns, Timer=TSC
.NET Core SDK=2.1.504
  [Host]     : .NET Core 2.1.8 (CoreCLR 4.6.27317.03, CoreFX 4.6.27317.03), 64bit RyuJIT  [AttachedDebugger]
  DefaultJob : .NET Core 2.1.8 (CoreCLR 4.6.27317.03, CoreFX 4.6.27317.03), 64bit RyuJIT


```
|         Method |      Mean |     Error |    StdDev |
|--------------- |----------:|----------:|----------:|
| BenchmarkRegex |  56.41 us | 0.1220 us | 0.1081 us |
|    BenchmarkSB | 599.81 us | 2.2549 us | 1.9989 us |
| BenchmarkArray | 140.14 us | 0.9896 us | 0.8772 us |
