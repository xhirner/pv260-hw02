﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using BenchmarkDotNet.Running;
using static System.String;

namespace XMLRepairTool
{
    public class RepairTool
    {
        private const string InvalidXmlCharactersRegex = @"[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]";

        private readonly char[] _invalidXmlCharactersArray;

        private readonly string[] _invalidXmlCharactersArrayStr;

        public RepairTool()
        {
            List<char> invalidChars = new List<char>();
            invalidChars.Add((char) 0x09);
            invalidChars.Add((char) 0x0A);
            invalidChars.Add((char) 0x0D);
            invalidChars.Add((char) 0x01);
            for (int i = 0x01; i < 0x20; i++)
            {
                invalidChars.Add((char) i);
            }

            _invalidXmlCharactersArray = invalidChars.ToArray();
            _invalidXmlCharactersArrayStr = invalidChars.Select(x => x.ToString()).ToArray();
        }

        public XmlDocument RepairAndParseXmlDocument(string filePath)
        {
            string raw = File.ReadAllText(filePath);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(RemoveInvalidXmlCharactersRegex(raw));
            return xml;
        }

        public string RemoveInvalidXmlCharactersRegex(string input)
        {
            return Regex.Replace(input, InvalidXmlCharactersRegex, Empty);
        }

        public string RemoveInvalidXmlCharactersSB(string input)
        {
            StringBuilder sb = new StringBuilder(input);
            for (var i = 0; i < _invalidXmlCharactersArray.Length; i++)
            {
                sb.Replace(_invalidXmlCharactersArrayStr[i], Empty);
            }
            return sb.ToString();
        }

        public string RemoveInvalidXmlCharactersArray(string input)
        {
            byte[] arr = Encoding.ASCII.GetBytes(input);
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < _invalidXmlCharactersArray.Length; j++)
                {
                    if (_invalidXmlCharactersArray[j] == arr[i])
                    {
                        arr[i] = 32;
                        break;
                    }
                }
            }

            return Encoding.ASCII.GetString(arr);
        }

        public static void Main(string[] args)
        {
            var results = BenchmarkRunner.Run<ToolBenchmark>();
            Console.ReadLine();
        }
    }
}
