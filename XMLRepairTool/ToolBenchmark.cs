﻿using System.IO;
using BenchmarkDotNet.Attributes;

namespace XMLRepairTool
{
    public class ToolBenchmark
    {
        private RepairTool _rt;
        private string _rawXml;

        [GlobalSetup]
        public void Setup()
        {
            _rt = new RepairTool();
            _rawXml = File.ReadAllText(@"C:\Users\Erik\source\repos\XMLRepairTool\XMLRepairTool\test.xml");
        }

        [Benchmark]
        public void BenchmarkRegex()
        {
            string s = _rt.RemoveInvalidXmlCharactersRegex(_rawXml);
        }

        [Benchmark]
        public void BenchmarkSB()
        {
            string s = _rt.RemoveInvalidXmlCharactersSB(_rawXml);
        }

        [Benchmark]
        public void BenchmarkArray()
        {
            string s = _rt.RemoveInvalidXmlCharactersArray(_rawXml);
        }
    }
}
